package io.pivotal.pal.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
    @GetMapping(path = "/demo")
    public String demo() {
        return "This is a demo";
    }

    @GetMapping(path = "/boom")
    public String boom() {
        throw new RuntimeException("Implement me!");
    }
}
