package io.pivotal.pal.demo;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class DemoConfig implements WebMvcConfigurer {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        // Map the root URL to the index template
        registry.addViewController("/").setViewName("index");
        registry.addStatusController("/public/ping", HttpStatus.OK);
    }
}
